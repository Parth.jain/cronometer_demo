package com.example.android.chronometer;

import android.animation.ObjectAnimator;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button mStartBtn,mStopBtn,mRestartBtn,mSetFormatBtn,mClearFormatBtn;
        final Chronometer chronometer;
        mStartBtn=findViewById(R.id.btnStart);
        mStopBtn=findViewById(R.id.btnStop);
        mRestartBtn=findViewById(R.id.btnRestart);
        mSetFormatBtn=findViewById(R.id.btnSetFormat);
        mClearFormatBtn=findViewById(R.id.btnClearFormat);

        chronometer=findViewById(R.id.SimpleChrono);

        mStartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chronometer.start();
            }
        });

        mStopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chronometer.stop();
            }
        });

        mRestartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chronometer.setBase(SystemClock.elapsedRealtime());
            }
        });

        mSetFormatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chronometer.setFormat("Time (%s)");
            }
        });

        mClearFormatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chronometer.setFormat(null);
            }
        });


        ObjectAnimator animator=ObjectAnimator.ofFloat(mStartBtn,"rotation",0,360);
        ObjectAnimator animator1=ObjectAnimator.ofFloat(mClearFormatBtn,"rotation",-360,0);
        animator.setDuration(1000);
        animator1.setDuration(1000);
        animator1.start();
        animator.start();


    }
}
